/*!
 *  @file      reconstruction.cpp
 *  @author    Alessio Piucci
 *  @brief     Reconstruction of the events
 */
 
//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <map>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRParticle.h"
#include "../include/TFRGeometry.h"
#include "../include/TFRTrack.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-g: config file name containing the geometry" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> reconstruction" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-g") == "")
       || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string configGeoFile_name = cmdline.GetArg("-g");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "configGeoFile_name = " << configGeoFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  //-----------------------//
  //  import the geometry  //
  //-----------------------//

  //create the detector geometry
  TFRGeometry *detector_geo = new TFRGeometry(configGeoFile_name);

  std::cout << "Geometry imported, TAG = " << detector_geo->GetTag() << std::endl;
  std::cout << "nLayers = " << detector_geo->GetNLayers() << std::endl;
  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  //test
  //Hit *test_hit = new Hit(detector_geo->GetLayers()->at(0));
  //Particle *test_particle = new Particle(TEveVectorD(0., 0., 0.), TEveVectorD(0., 0., 0.), 0.);
  //test_particle->AddHit(test_hit);
  
  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;
  
  //loop over the events                                                                                                                                                                            
  while((curr_event = (TFREvent*) it_event.Next())){

    //for now, let's skip the reconstruction
    //I will create a track for each single MC particle:
    //it's like having 100% reconstruction efficiency, and 0% reconstruction ghost rate
    
    /*
    //-----------------//
    //  hit deconding  //
    //-----------------//
    
    //get the clusters of the current event
    TFRClusters *cluster_list = (TFRClusters*) curr_event->GetClusters();
    
    std::cout << "evtID = " << curr_event->GetID()
	      << ", num_clusters = " << cluster_list->GetEntries() << std::endl;
    
    //define a <layer, clusters> map to easily handle the clusters in the reconstruction
    std::map<TFRLayer, TFRClusters> cluster_map;
    
    //loop over the detector layers, to create the keys of the map (i.e. indexing by layers)

    //I've no clue why the hell in ROOT there aren't nice implementations of iterators, 
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_layer((TFRLayers*) detector_geo->GetLayers());
    TFRLayer *curr_layer;
    
    //loop over detector layers
    while((curr_layer = (TFRLayer*) it_layer.Next()))
      cluster_map.insert(std::make_pair(*curr_layer, TFRClusters{}));
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_cluster((TFRClusters*) cluster_list);
    TFRCluster *curr_cluster;

    //loop over the clusters, to map them
    while((curr_cluster = (TFRCluster*) it_cluster.Next())){
      
      //map the clusters to the current layers
      (*(cluster_map.find(curr_cluster->GetLayer()))).second.Add(curr_cluster);
      
    }  //loop over the clusters
    
    */
    
    /*
    //debug output:: loop over the indexed layers
    for(std::map<TFRLayer, TFRClusters>::iterator it_map = cluster_map.begin();
	it_map != cluster_map.end(); ++it_map){
      
      std::cout << "layer_id = " << ((*it_map).first).GetID() << std::endl;
      
      //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
      //but just this stupid way (that I even didn't used at my C course during my bachelor)
      TIter it_cluster_debug((TFRClusters*) &((*it_map).second));
      TFRCluster *curr_cluster_debug;
      
      //loop over the indexed clusters
      while((curr_cluster_debug = (Cluster*) it_cluster_debug.Next()))
      	std::cout << "---> x = (" << curr_cluster_debug->GetXMax() << ", " << curr_cluster_debug->GetXMin() << "), "
		  << "y = (" << curr_cluster_debug->GetYMax() << ", " << curr_cluster_debug->GetYMin() << "), "
		  << "z = " << curr_cluster_debug->GetZ() << std::endl;
      
    }  //loop over the indexed layers
    */

    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_particle((TFRParticles*) curr_event->GetGenParticles());
    TFRParticle *curr_particle;

    //loop over particles
    while((curr_particle = (TFRParticle*) it_particle.Next())){

      //skip particle with null charge
      if(curr_particle->GetCharge() == 0.)
	continue;
      
      //create a reconstructed track
      TFRTrack *curr_track = new TFRTrack();

      //get the clusters of the current particle
      TFRClusters *cluster_list = (TFRClusters*) curr_particle->GetClusters();
      //get a clone, I don't want to play with the original copy
      cluster_list = (TFRClusters*) cluster_list->Clone();
      cluster_list->SetOwner(kTRUE);
      
      //associate the track to the MC particle
      curr_track->AddParticle(curr_particle);

      //cheat a little bit and set the true momentum
      curr_track->SetMomentum(curr_particle->GetMomentum());
      
      //set the clusters to the track
      curr_track->SetClusters(cluster_list);

      //add the track to the event
      curr_event->AddRecoTrack(curr_track);
      
    }  //loop over particles
  }  //loop over the events
  
  //write the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  outFile->Write();
  
  return 0;
  
}
