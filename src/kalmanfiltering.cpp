/*!
 *  @file      kalmanfiltering.cpp
 *  @author    Simon Stemmle
 *  @brief     Fitting tracks with a kalman filter
 */
 
//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <map>
#include <chrono>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRGeometry.h"
#include "../include/TFRKalmanFilter.h"
#include "../include/TFRTrack.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-g: config file name containing the geometry" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> kalman filtering" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-g") == "")
       || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string configGeoFile_name = cmdline.GetArg("-g");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "configGeoFile_name = " << configGeoFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  //-----------------------//
  //  import the geometry  //
  //-----------------------//

  //create the detector geometry
  TFRGeometry *detector_geo = new TFRGeometry(configGeoFile_name);

  std::cout << "Geometry imported, TAG = " << detector_geo->GetTag() << std::endl;
  std::cout << "nLayers = " << detector_geo->GetNLayers() << std::endl;
  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;
  
  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//
  
  //Create a Kalman Filter tool using the default propagator with multiple scattering
  TFRPropagator *propagator = new TFRPropagator(detector_geo,true);
  TFRKalmanFilter *kalmanfilter = new TFRKalmanFilter(propagator);
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;

  //start to measure the timing
  std::chrono::high_resolution_clock::time_point t_start = std::chrono::high_resolution_clock::now();
  
  //loop over the events
  while((curr_event = (TFREvent*) it_event.Next())){

    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_track((TFRTracks*) curr_event->GetRecoTracks());
    TFRTrack *curr_track;

    //loop over tracks
    while((curr_track = (TFRTrack*) it_track.Next())){

      //fit the current track with the Kalman Filter
      kalmanfilter->FitTrack(curr_track);
      
    }  //loop over particles
  }  //loop over the events
  
  //end to measure the timing
  std::chrono::high_resolution_clock::time_point t_end = std::chrono::high_resolution_clock::now();

  //print the timing
  std::cout << "total timing = "
	    << ((std::chrono::duration_cast<std::chrono::microseconds>(t_end - t_start).count())/1000.)/event_list->GetEntries()
	    << " ms/event." << std::endl;
  
  //write the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  outFile->Write();
  
  return 0;
  
}
