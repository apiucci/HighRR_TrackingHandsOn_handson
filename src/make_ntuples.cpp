/*!
 *  @file      make_ntuples.cpp
 *  @author    Alessio Piucci
 *  @brief     Macro to store the output of the simulation into ntuples
 */
 
//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRParticle.h"
#include "../include/TFRTrack.h"

/*! Maximum number of layers */
const unsigned int max_num_layers = 30;

/*! Maximum number of generated particles associated to a reconstructed track */
const unsigned int max_num_particles = 5;

using namespace std;

/*! Print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> make_ntuples" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");

  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  
  //----------------------------//
  //  sset the output branches  //
  //----------------------------//

  //event variables
  int evtID;
  //unsigned int nParticles;
  
  //particle variables
  int particle_ID;
  double particle_vx, particle_vy, particle_vz;
  double particle_px, particle_py, particle_pz;
  int particle_charge;
  double particle_mass;

  double particle_reco_vx, particle_reco_vy, particle_reco_vz;
  double particle_reco_px, particle_reco_py, particle_reco_pz;
  int particle_reco_charge;
  double particle_reco_mass;
  
  //particle hits
  int particle_hit_layerID[max_num_layers] = {-999999};
  double particle_hit_x[max_num_layers] = {0.};
  double particle_hit_y[max_num_layers] = {0.};
  double particle_hit_z[max_num_layers] = {0.};

  //particle clusters
  int particle_cluster_layerID[max_num_layers] = {-999999};
  double particle_cluster_x[max_num_layers] = {0.};
  double particle_cluster_y[max_num_layers] = {0.};
  double particle_cluster_z[max_num_layers] = {0.};

  //track quantities
  //Chi2
  double track_vx, track_vy, track_vz;
  double track_vx_err, track_vy_err, track_vz_err;
  double track_px, track_py, track_pz, track_p;
  double track_tx, track_ty;
  double track_tx_err, track_ty_err, track_p_err;
  int track_charge;
 
  unsigned int track_numClusters;
  //Kalman
  double track_chi2, track_chi2NdoF, track_pval;
  double track_vx_km, track_vy_km, track_vz_km;
  double track_vx_km_err, track_vy_km_err, track_vz_km_err;
  double track_px_km, track_py_km, track_pz_km, track_p_km;
  double track_tx_km, track_ty_km;
  double track_tx_km_err, track_ty_km_err, track_p_km_err;
  int track_charge_km;
  double track_chi2_km, track_chi2NdoF_km, track_pval_km;
  
  //particles associated to a given track
  double particle_vx_array[max_num_particles] =	{-999999.};
  double particle_vy_array[max_num_particles] = {-999999.};
  double particle_vz_array[max_num_particles] = {-999999.};
  
  double particle_tx_array[max_num_particles] =	{-999999.};
  double particle_ty_array[max_num_particles] = {-999999.};
  
  double particle_px_array[max_num_particles] = {-999999.};
  double particle_py_array[max_num_particles] =	{-999999.};
  double particle_pz_array[max_num_particles] = {-999999.};
  double particle_p_array[max_num_particles] = {-999999.};

  int particle_ID_array[max_num_particles] = {-999999};
  int particle_charge_array[max_num_particles] = {-999999};
  
  //default arrays to set unset variables
  double unset_max_num_layer[max_num_layers] = {0.};
  double unset_max_num_particles[max_num_particles] = {0.};

  outFile->cd();

  //tree with the generated quantities
  TTree *particle_tree = new TTree("particle_tree", "particle_tree");

  particle_tree->Branch("evtID", &evtID, "evtID/I");

  particle_tree->Branch("particle_ID", &particle_ID, "particle_ID/I");
  particle_tree->Branch("particle_vx", &particle_vx, "particle_vx/D");
  particle_tree->Branch("particle_vy", &particle_vy, "particle_vy/D");
  particle_tree->Branch("particle_vz", &particle_vz, "particle_vz/D");
  particle_tree->Branch("particle_px", &particle_px, "particle_px/D");
  particle_tree->Branch("particle_py", &particle_py, "particle_py/D");
  particle_tree->Branch("particle_pz", &particle_pz, "particle_pz/D");
  particle_tree->Branch("particle_charge", &particle_charge, "particle_charge/I");

  //TO UNDERSTAND: why the hell I have to specify a const number, and I cannot assign the compiler-constant max_num_layers
  particle_tree->Branch("particle_MCHit_layerID", particle_hit_layerID, "particle_MCHit_layerID[30]/I");
  particle_tree->Branch("particle_MCHit_x", particle_hit_x, "particle_MCHit_x[30]/D");
  particle_tree->Branch("particle_MCHit_y", particle_hit_y, "particle_MCHit_y[30]/D");
  particle_tree->Branch("particle_MCHit_z", particle_hit_z, "particle_MCHit_z[30]/D");

  particle_tree->Branch("particle_cluster_layerID", particle_cluster_layerID, "particle_cluster_layerID[30]/I");
  particle_tree->Branch("particle_cluster_x", particle_cluster_x, "particle_cluster_x[30]/D");
  particle_tree->Branch("particle_cluster_y", particle_cluster_y, "particle_cluster_y[30]/D");
  particle_tree->Branch("particle_cluster_z", particle_cluster_z, "particle_cluster_z[30]/D");
  
  //tree with the reconstructed quantities
  TTree *track_tree = new TTree("track_tree", "track_tree");

  track_tree->Branch("evtID", &evtID, "evtID/I");
  
  track_tree->Branch("track_numClusters" ,&track_numClusters,"track_numClusters/i");
  
  //chi2 fit
  track_tree->Branch("track_vx"          ,&track_vx         ,"track_vx/D");
  track_tree->Branch("track_vy"          ,&track_vy         ,"track_vy/D");
  track_tree->Branch("track_vz"          ,&track_vz         ,"track_vz/D");
  track_tree->Branch("track_vx_err"      ,&track_vx_err     ,"track_vx_err/D");
  track_tree->Branch("track_vy_err"      ,&track_vy_err     ,"track_vy_err/D");
  track_tree->Branch("track_vz_err"      ,&track_vz_err     ,"track_vz_err/D");
  track_tree->Branch("track_tx"          ,&track_tx         ,"track_tx/D");
  track_tree->Branch("track_ty"          ,&track_ty         ,"track_ty/D");
  track_tree->Branch("track_tx_err"      ,&track_tx_err     ,"track_tx_err/D");
  track_tree->Branch("track_ty_err"      ,&track_ty_err     ,"track_ty_err/D");
  track_tree->Branch("track_px"          ,&track_px         ,"track_px/D");
  track_tree->Branch("track_py"          ,&track_py         ,"track_py/D");
  track_tree->Branch("track_pz"          ,&track_pz         ,"track_pz/D");
  track_tree->Branch("track_p"           ,&track_p          ,"track_p/D");
  track_tree->Branch("track_p_err"       ,&track_p_err      ,"track_p_err/D");
  track_tree->Branch("track_charge"      ,&track_charge     ,"track_charge/I");
  track_tree->Branch("track_chi2"        ,&track_chi2       ,"track_chi2/D");
  track_tree->Branch("track_chi2NodF"    ,&track_chi2NdoF   ,"track_chi2NdoF/D");
  track_tree->Branch("track_pval"        ,&track_pval       ,"track_pval/D");
  
  //kalman filter
  track_tree->Branch("track_vx_km"       ,&track_vx_km        ,"track_vx_km/D");
  track_tree->Branch("track_vy_km"       ,&track_vy_km        ,"track_vy_km/D");
  track_tree->Branch("track_vz_km"       ,&track_vz_km        ,"track_vz_km/D");
  track_tree->Branch("track_vx_km_err"   ,&track_vx_km_err    ,"track_vx_km_err/D");
  track_tree->Branch("track_vy_km_err"   ,&track_vy_km_err    ,"track_vy_km_err/D");
  track_tree->Branch("track_vz_km_err"   ,&track_vz_km_err    ,"track_vz_km_err/D");
  track_tree->Branch("track_tx_km"       ,&track_tx_km        ,"track_tx_km/D");
  track_tree->Branch("track_ty_km"       ,&track_ty_km        ,"track_ty_km/D");
  track_tree->Branch("track_tx_km_err"   ,&track_tx_km_err    ,"track_tx_km_err/D");
  track_tree->Branch("track_ty_km_err"   ,&track_ty_km_err    ,"track_ty_km_err/D");
  track_tree->Branch("track_px_km"       ,&track_px_km        ,"track_px_km/D");
  track_tree->Branch("track_py_km"       ,&track_py_km        ,"track_py_km/D");
  track_tree->Branch("track_pz_km"       ,&track_pz_km        ,"track_pz_km/D");
  track_tree->Branch("track_p_km"        ,&track_p_km         ,"track_p_km/D");
  track_tree->Branch("track_p_km_err"    ,&track_p_km_err     ,"track_p_km_err/D");
  track_tree->Branch("track_charge_km"   ,&track_charge_km    ,"track_charge_km/I");
  track_tree->Branch("track_chi2_km"     ,&track_chi2_km      ,"track_chi2_km/D");
  track_tree->Branch("track_chi2NodF_km" ,&track_chi2NdoF_km  ,"track_chi2NdoF_km/D");
  track_tree->Branch("track_pval_km"     ,&track_pval_km      ,"track_pval_km/D");
  
  track_tree->Branch("particle_ID", particle_ID_array, "particle_ID[5]/I");
  track_tree->Branch("particle_vx", particle_vx_array, "particle_vx[5]/D");
  track_tree->Branch("particle_vy", particle_vy_array, "particle_vy[5]/D");
  track_tree->Branch("particle_vz", particle_vz_array, "particle_vz[5]/D");
  track_tree->Branch("particle_tx", particle_tx_array, "particle_tx[5]/D");
  track_tree->Branch("particle_ty", particle_ty_array, "particle_ty[5]/D");
  track_tree->Branch("particle_px", particle_px_array, "particle_px[5]/D");
  track_tree->Branch("particle_py", particle_py_array, "particle_py[5]/D");
  track_tree->Branch("particle_pz", particle_pz_array, "particle_pz[5]/D");
  track_tree->Branch("particle_p" , particle_p_array , "particle_p[5]/D");
  track_tree->Branch("particle_charge", particle_charge_array, "particle_charge[5]/I");

  //tree with the signal decay informations
  TTree *decay_tree = new TTree("decay_tree", "decay_tree");
  
  decay_tree->Branch("particle_gen_vx", &particle_vx, "particle_gen_vx/D");
  decay_tree->Branch("particle_gen_vy", &particle_vy, "particle_gen_vy/D");
  decay_tree->Branch("particle_gen_vz", &particle_vz, "particle_gen_vz/D");
  decay_tree->Branch("particle_gen_px", &particle_px, "particle_gen_px/D");
  decay_tree->Branch("particle_gen_py", &particle_py, "particle_gen_py/D");
  decay_tree->Branch("particle_gen_pz", &particle_pz, "particle_gen_pz/D");
  decay_tree->Branch("particle_gen_mass", &particle_mass, "particle_gen_mass/D");
  decay_tree->Branch("particle_gen_charge", &particle_charge, "particle_gen_charge/I");

  decay_tree->Branch("particle_reco_vx", &particle_reco_vx, "particle_reco_vx/D");
  decay_tree->Branch("particle_reco_vy", &particle_reco_vy, "particle_reco_vy/D");
  decay_tree->Branch("particle_reco_vz", &particle_reco_vz, "particle_reco_vz/D");
  decay_tree->Branch("particle_reco_px", &particle_reco_px, "particle_reco_px/D");
  decay_tree->Branch("particle_reco_py", &particle_reco_py, "particle_reco_py/D");
  decay_tree->Branch("particle_reco_pz", &particle_reco_pz, "particle_reco_pz/D");
  decay_tree->Branch("particle_reco_mass", &particle_reco_mass, "particle_reco_mass/D");
  decay_tree->Branch("particle_reco_charge", &particle_reco_charge, "particle_reco_charge/I");
  
  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;
  
  //loop over the events
  while((curr_event = (TFREvent*) it_event.Next())){

    //std::cout << "evtID = " << curr_event->GetID() << std::endl;

    evtID = curr_event->GetID();

    //----------------------------------//
    //  store the generated quantities  //
    //----------------------------------//
    
    //get the particles of the current event
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_particle((TFRParticles*) curr_event->GetGenParticles());
    TFRParticle *curr_particle;
    
    //loop over the particles of the event
    while((curr_particle = (TFRParticle*) it_particle.Next())){

      //set the particle variables that will be stored
      particle_ID = curr_particle->GetID();
      particle_vx = curr_particle->GetVertex()[0];
      particle_vy = curr_particle->GetVertex()[1];
      particle_vz = curr_particle->GetVertex()[2];

      particle_px = curr_particle->GetMomentum()[0];
      particle_py = curr_particle->GetMomentum()[1];
      particle_pz = curr_particle->GetMomentum()[2];

      particle_charge = curr_particle->GetCharge();

      unsigned int i_array = 0;

      //reset the hit and cluster arrays
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_hit_layerID);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_hit_x);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_hit_y);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_hit_z);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_cluster_layerID);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_cluster_x);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_cluster_y);
      std::copy(unset_max_num_layer, unset_max_num_layer + max_num_layers, particle_cluster_z);
      
      //get the MC hits of the current particle
      
      //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
      //but just this stupid way (that I even didn't used at my C course during my bachelor)
      TIter it_hit((TFRMCHits*) curr_particle->GetHits());
      TFRMCHit *curr_hit;

      //loop over the MC hits of the current particle
      while((curr_hit = (TFRMCHit*) it_hit.Next())){
	
	//set the hit variables
	particle_hit_layerID[i_array] = curr_hit->GetLayer().GetID();
	particle_hit_x[i_array] = curr_hit->GetPosition()[0];
	particle_hit_y[i_array] = curr_hit->GetPosition()[1];
	particle_hit_z[i_array] = curr_hit->GetPosition()[2];

	++i_array;
      }  //loop over the hits of the current particle

      //get the clusters of the current particle
      
      //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
      //but just this stupid way (that I even didn't used at my C course during my bachelor)
      TIter it_cluster((TFRClusters*) curr_particle->GetClusters());
      TFRCluster *curr_cluster;

      i_array = 0;
      
      //loop over the clusters of the current particle
      while((curr_cluster = (TFRCluster*) it_cluster.Next())){
	
        //set the cluster variables
        particle_cluster_layerID[i_array] = curr_cluster->GetLayer().GetID();
	particle_cluster_x[i_array] = curr_cluster->GetX();
	particle_cluster_y[i_array] = curr_cluster->GetY();
	particle_cluster_z[i_array] = curr_cluster->GetZ();

	++i_array;
	
      }  //loop over the clusters of the current particle                                                                                                                                                           
      //fill the particle tree
      particle_tree->Fill();
      
    }  //loop over the particles of the event


    //--------------------------------------//
    //  store the reconstructed quantities  //
    //--------------------------------------//

    //get the reconstructed tracks of the event
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_track((TFRTracks*) curr_event->GetRecoTracks());
    TFRTrack *curr_track;

    //loop over the reconstructed tracks of the event
    while((curr_track = (TFRTrack*) it_track.Next())){
      track_numClusters = curr_track->GetNClusters();

      //chi2 fit
      TFRState *state = curr_track->GetInitialState();

      if(state != NULL){
        track_vx = state->GetStateVect()[0];
        track_vy = state->GetStateVect()[1];
        track_vz = state->GetZ();
        
        track_vx_err = sqrt(state->GetP()[0][0]);
        track_vy_err = sqrt(state->GetP()[1][1]);
        track_vz_err = 0;
        
        track_tx = state->GetStateVect()[2];
        track_ty = state->GetStateVect()[3];
        
        track_tx_err = sqrt(state->GetP()[2][2]);
        track_ty_err = sqrt(state->GetP()[3][3]);
        
        track_px = state->GetMomentum()[0];
        track_py = state->GetMomentum()[1];
        track_pz = state->GetMomentum()[2];
        track_p  = 1.0/abs(state->GetQoP());
        track_p_err = sqrt(state->GetP()[4][4])/fabs(state->GetQoP())*track_p;
        track_charge = curr_track->GetCharge()>0? 1 : -1;
        
        track_chi2 = curr_track->GetChi2();
        track_chi2NdoF = curr_track->GetChi2NdoF();
        track_pval = TMath::Prob(track_chi2,curr_track->GetNdoF());
      }
      
      //kalman filter
      //Get the state at z=0
      TFRState *statekm = curr_track->GetKalmanFilteredStateAt0();
      if(statekm != NULL){
        track_vx_km = statekm->GetStateVect()[0];
        track_vy_km = statekm->GetStateVect()[1];
        track_vz_km = statekm->GetZ();
        
        track_vx_km_err = sqrt(statekm->GetP()[0][0]);
        track_vy_km_err = sqrt(statekm->GetP()[1][1]);
        track_vz_km_err = 0;
        
        track_tx_km = statekm->GetStateVect()[2];
        track_ty_km = statekm->GetStateVect()[3];
        
        track_tx_km_err = sqrt(statekm->GetP()[2][2]);
        track_ty_km_err = sqrt(statekm->GetP()[3][3]);
        
        track_px_km = statekm->GetMomentum()[0];
        track_py_km = statekm->GetMomentum()[1];
        track_pz_km = statekm->GetMomentum()[2];
        track_p_km  = 1.0/abs(statekm->GetQoP());
        track_p_km_err = sqrt(statekm->GetP()[4][4])/fabs(statekm->GetQoP())*track_p_km;
        
        track_charge_km = statekm->GetQoP()>0? 1 : -1;
        
        track_chi2_km = curr_track->GetChi2(true);
        track_chi2NdoF_km = curr_track->GetChi2NdoF(true);
        track_pval_km = TMath::Prob(track_chi2_km,curr_track->GetNdoF(true));
      }
      //reset the particle arrays
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_ID_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_charge_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_vx_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_vy_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_vz_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_tx_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_ty_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_px_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_py_array);
      std::copy(unset_max_num_particles, unset_max_num_particles + max_num_particles, particle_pz_array);

      //now store the informations related to the particles
      //associated to the current reconstructed track

      //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
      //but just this stupid way (that I even didn't used at my C course during my bachelor)
      TIter it_particle_track((TFRParticles*) curr_track->GetParticles());
      TFRParticle *curr_particle_track;

      unsigned int i_particle = 0;
      
      //loop over the particles associated to the track
      while((curr_particle_track = (TFRParticle*) it_particle_track.Next())){

	particle_ID_array[i_particle] = curr_particle_track->GetID();
	
	particle_vx_array[i_particle] = curr_particle_track->GetVertex()[0];
	particle_vy_array[i_particle] = curr_particle_track->GetVertex()[1];
	particle_vz_array[i_particle] = curr_particle_track->GetVertex()[2];

	particle_tx_array[i_particle] = curr_particle_track->GetTx();
	particle_ty_array[i_particle] = curr_particle_track->GetTy();

	particle_px_array[i_particle] = curr_particle_track->GetMomentum()[0];
        particle_py_array[i_particle] = curr_particle_track->GetMomentum()[1];
        particle_pz_array[i_particle] = curr_particle_track->GetMomentum()[2];
        particle_p_array[i_particle] = curr_particle_track->GetMomentum().Mag();

	particle_charge_array[i_particle] = curr_particle_track->GetCharge();
	
	++i_particle;
      }  //loop over the particles associated to the track

      //fill the track tree
      track_tree->Fill();
      
    }  //loop over the reconstructed tracks of the event

    
    //-----------------//
    //  signal decays  //
    //-----------------//

    //get the particles of the current event:
    //I need to retrieve the simulated signal particle
    //let's assume we only have a signal particle per event
    
    //reset the particle iterator
    it_particle.Reset();
    
    //loop over the particles of the event
    while((curr_particle = (TFRParticle*) it_particle.Next())){

      //is the current particle the signal one?
      if(curr_particle->IsSignal()){
	
	//set the particle variables that will be stored
	particle_ID = curr_particle->GetID();
	particle_vx = curr_particle->GetVertex()[0];
	particle_vy = curr_particle->GetVertex()[1];
	particle_vz = curr_particle->GetVertex()[2];
	
	particle_px = curr_particle->GetMomentum()[0];
	particle_py = curr_particle->GetMomentum()[1];
	particle_pz = curr_particle->GetMomentum()[2];
	
	particle_charge = curr_particle->GetCharge();
	particle_mass = curr_particle->GetMass();
	
	break;
	
      }  //if(curr_particle->IsSignal())
    }  //loop over the particles of the event

    //now get the reconstructed signal particle
    particle_reco_vx = curr_event->GetRecoSignalParticle()->GetVertex()[0];
    particle_reco_vy = curr_event->GetRecoSignalParticle()->GetVertex()[1];
    particle_reco_vz = curr_event->GetRecoSignalParticle()->GetVertex()[2];

    particle_reco_px = curr_event->GetRecoSignalParticle()->GetMomentum()[0];
    particle_reco_py = curr_event->GetRecoSignalParticle()->GetMomentum()[1];
    particle_reco_pz = curr_event->GetRecoSignalParticle()->GetMomentum()[2];

    particle_reco_charge = curr_event->GetRecoSignalParticle()->GetCharge();
    particle_reco_mass = curr_event->GetRecoSignalParticle()->GetMass();

    decay_tree->Fill();
    
  }  //loop over the events
  
  //write the output file
  outFile->cd();
  particle_tree->Write();
  track_tree->Write();
  decay_tree->Write();
  outFile->Write();
  
  return 0;
  
}
