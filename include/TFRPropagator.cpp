/*!
 *  @author    Simon Stemmle
 *  @brief     Extrapolator of a track state to a given z position
 */

#include "TFRPropagator.h"

using namespace std;

//----------
// Extrapolate a state to a given z position
//----------
bool TFRPropagator::PropagateState(TFRState *state, double z){

  //Get the layers which are between initial and final z (state->GetZ() <= z_layer <z)
  TFRLayers layers = m_geo->GetLayersBetween(state->GetZ(), z);
 
  //Do the extrapolation from layer to layer
  for(int i = 0; i < layers.GetEntries(); i++){
    
    //Do the actual extrapolation to the next layer
    if(!PropagateStateNoScattering(state, ((TFRLayer*) layers.At(i))->GetPosition()[2]))
      return false;

    //Add uncertainty due to multiple scattering at this layer
    if(!AddScatteringToCovariance(state, (TFRLayer*) layers.At(i)))
      return false;
  }
  
  //Do the remaining extrapolation after the last layer
  if(!PropagateStateNoScattering(state, z))
    return false;

  return true;
}

//----------
// Extrapolate a state to a given z position without any scattering inbetween
//----------
bool TFRPropagator::PropagateStateNoScattering(TFRState *state, double z){

  //If we are already there we have nothing to do
  if(z==(state->GetZ())) return true;

  //Determine wether we are propagating fowards or backwards
  bool forward = z > state->GetZ();

  //Check wether there is a magnetic field change inbetween (entering and/or leaving it)
  if(forward){

    //If the magnetic field will come at larger z
    if(m_geo->GetBFieldStartZ() >= state->GetZ()){
      PropagateStateStraight(state, min(z,m_geo->GetBFieldStartZ()));
      
      //inside the possible B-field
      if(!PropagateStateInBField(state, min(z,m_geo->GetBFieldEndZ()))) return false;
    }
    
    //If we are already in the B-field
    else if(m_geo->GetBFieldEndZ() >= state->GetZ()){
      if(!PropagateStateInBField(state, min(z,m_geo->GetBFieldEndZ()))) return false;
    }

    //Straight in the possible rest after the B-field
    PropagateStateStraight(state, z);
  }
  else{
    //If the B-field will come at smaller z
    if(m_geo->GetBFieldEndZ() <= state->GetZ()){
      PropagateStateStraight(state, max(z,m_geo->GetBFieldEndZ()));
      
      //inside the possible B-field
      if(!PropagateStateInBField(state, max(z,m_geo->GetBFieldStartZ())))
        return false;
    }
    
    //If we are already in the B-field
    else if(m_geo->GetBFieldStartZ() <= state->GetZ()){
      if(!PropagateStateInBField(state, max(z,m_geo->GetBFieldStartZ()))) return false;
    }
    
    
    //Straight in the possible rest after the B-field
    PropagateStateStraight(state, z);
    
  }
  
  return true;
}
//----------
// Extrapolate a state to a given z position (straight line)
//----------
void TFRPropagator::PropagateStateStraight(TFRState *state, double z){
  
  //Distance in z to be extrapolated
  double dz = z-state->GetZ();
  
  //If we are already there, we have nothing to do
  if(dz==0) return;

  //Transportation matrix: x_new=F*x_old
  //Array representing this matrix
  double FtmpA[5*5] = {1, 0,dz, 0, 0,
                       0, 1, 0,dz, 0,
                       0, 0, 1, 0, 0,
                       0, 0, 0, 1, 0,
                       0, 0, 0, 0, 1};

  TMatrixD Ftmp(5,5,FtmpA);
  
  //Transposed matrix
  TMatrixD FtmpT(TMatrixD::kTransposed, Ftmp);

  //Transport the statevector with a stright line
  state->SetStateVect(Ftmp * (state->GetStateVect()));
  
  //Transport the covariance matrix using the same transportation matrix
  state->SetP(Ftmp * (state->GetP()) * FtmpT); 

  //Set the new z position
  state->SetZ(z);

  return;
}

//----------
// Extrapolate a state to a given z position (in constant B-field)
//----------
bool TFRPropagator::PropagateStateInBField(TFRState *state, double z){

  //distance in z
  double dz = z-state->GetZ();
  
  //If we are already there we have nothing to do
  if(dz==0) return true;
  
  //if B-field strength is zero: do the straight line extrapolation
  if(m_geo->GetBMag()==0){
    PropagateStateStraight(state, z);
    return true;
  }
  
  //The state vector of the extrapolated state
  TVectorD state_new(5);

  //Current state vector
  TVectorD state_vect = state->GetStateVect();

  //Extrapolation for y,ty: straight line (B-field only in y direction)
  state_new[1] = state_vect[1] + dz * state_vect[3];
  state_new[3] = state_vect[3]; 
  
  //tx: tx/sqrt(1+tx^2+ty^2)-txnew/sqrt(1+txnew^2+tynew^2) = q/p * B * dz
  //intermediate quantity: a=tx/sqrt(1+tx^2+ty^2) - q/p * B * dz
  double a = state_vect[2]/sqrt(1+state_vect[2]*state_vect[2]+state_vect[3]*state_vect[3]) 
             + state_vect[4] * m_geo->GetBMag() * dz * 299792485*1e-8;

  //check whether the track is reflected
  if(fabs(a)>=1){
    return false;
  }

  state_new[2] = a * sqrt((1+state_vect[3]*state_vect[3])/(1-a*a));
  
  //x: straight line to a certain position dzmag with tx and than the rest with txnew
  //this is a single kink in the middle of the field. 
  //Not exactly in the middle, we first have to calculate the exact position
  double dzmag = dz * (
                        state_new[2]*state_new[2]
                        +1
                        -sqrt((1+state_new[2]*state_new[2])*(1+state_vect[2]*state_vect[2]))
                      )
                      /(state_new[2]*state_new[2]-state_vect[2]*state_vect[2]);

  state_new[0] = state_vect[0] + dzmag*state_vect[2] + (dz-dzmag)*state_new[2];
  
  //q/p: no change since we assume that we have no energy loss
  state_new[4] = state_vect[4];
  
  //Set the new state vector
  state->SetStateVect(state_new);
  
  //determine the transport matrix (derivatives of the formulas above)
  TMatrixD Ftmp(5,5);

  //ty
  Ftmp(3,0) = 0;
  Ftmp(3,1) = 0;
  Ftmp(3,2) = 0;
  Ftmp(3,3) = 1;
  Ftmp(3,4) = 0;

  //y
  Ftmp(1,0) = 0;
  Ftmp(1,1) = 1;
  Ftmp(1,2) = 0;
  Ftmp(1,3) = dz;
  Ftmp(1,4) = 0;

  //tx
  double d_tx_d_a = sqrt( (1-a*a)*(1+state_vect[3]*state_vect[3]) )
                    /( (a*a-1)*(a*a-1) );
  double d_tx_d_ty= a*state_vect[3]
                    /sqrt((1-a*a)*(1+state_vect[3]*state_vect[3]));
  double d_a_d_tx = (1+state_vect[3]*state_vect[3])
                    /pow(1+state_vect[2]*state_vect[2]+state_vect[3]*state_vect[3],1.5);
  double d_a_d_ty = -state_vect[3]*state_vect[3]
                    /pow(1+state_vect[2]*state_vect[2]+state_vect[3]*state_vect[3],1.5);
  double d_a_d_qop= m_geo->GetBMag() * dz * 299792485*1e-8;

  Ftmp(2,0) = 0;
  Ftmp(2,1) = 0;
  Ftmp(2,2) = d_tx_d_a*d_a_d_tx;
  Ftmp(2,3) = d_tx_d_a*d_a_d_ty + d_tx_d_ty;
  Ftmp(2,4) = d_tx_d_a*d_a_d_qop;

  //x = state_new[0] = state_vect[0] + dzmag*state_vect[2] + (dz-dzmag)*state_new[2];
  double d_x_d_tx        = dzmag;
  double d_x_d_txnew     = dz-dzmag;
  double d_x_d_dzmag     = (state_vect[2]-state_new[2]);
  double d_dzmag_d_txnew = dz*2*state_new[2]/(state_new[2]*state_new[2]-state_vect[2]*state_vect[2])*
                           (
                            1
                            -dzmag/dz
                            -(1+state_vect[2]*state_vect[2])
                             /(2*sqrt((1+state_vect[2]*state_vect[2])*(1+state_new[2]*state_new[2])))
                           );
  double d_dzmag_d_tx = dz*2*state_vect[2]/(state_new[2]*state_new[2]-state_vect[2]*state_vect[2])*
                           (
                            dzmag/dz
                            -(1+state_new[2]*state_new[2])
                             /(2*sqrt((1+state_vect[2]*state_vect[2])*(1+state_new[2]*state_new[2])))
                           );
  Ftmp(0,0) = 1;
  Ftmp(0,1) = 0;
  Ftmp(0,2) = d_x_d_tx
             +d_x_d_txnew*Ftmp(2,2)
             +d_x_d_dzmag*(d_dzmag_d_tx + d_dzmag_d_txnew * Ftmp(2,2));
  Ftmp(0,3) = d_x_d_txnew*Ftmp(2,3)
             +d_x_d_dzmag*d_dzmag_d_txnew * Ftmp(2,3);
  Ftmp(0,4) = d_x_d_txnew*Ftmp(2,4)
             +d_x_d_dzmag*d_dzmag_d_txnew*Ftmp(2,4);

  //q/p
  Ftmp(4,0) = 0;
  Ftmp(4,1) = 0;
  Ftmp(4,2) = 0;
  Ftmp(4,3) = 0;
  Ftmp(4,4) = 1;
  

  //Transposed transport matrix
  TMatrixD FtmpT(TMatrixD::kTransposed, Ftmp);

  //Set the transported covariance matrix
  state->SetP(Ftmp * (state->GetP()) * FtmpT);
 
  //Set the new z position
  state->SetZ(z);
 
  return true;
}

//----------
// Add scattering to covariance matrix
//----------
bool TFRPropagator::AddScatteringToCovariance(TFRState *state, TFRLayer *layer){
  
  //check wether we are still inside the detector
  double x = (state->GetStateVect())[0];
  double y = (state->GetStateVect())[1];
  double xmax = layer->GetXMax();
  double xmin = layer->GetXMin();
  double ymax = layer->GetYMax();
  double ymin = layer->GetYMin();
  
  if(x < xmin || x > xmax || y < ymin || y > ymax) return false;

  //Check wether we should calculate the effect of MS at this point.
  if(!m_useMs || !m_geo->GetMultipleScattStatus()) return true;

  //Get multiple scattering factor (how much material)
  double XoverXZero = layer->GetXoverXZero();
  
  //Calculate the expected angular deflection
  double momentum = fabs(1.0/(state->GetStateVect())[4]);
  
  //assume kaon mass
  double beta = sqrt(1. - pow(493.7/momentum, 2));
  
  double angleRms = m_geo->GetMultipleScattering()->GetThetaRMSGauss(XoverXZero, beta, momentum, 1); 
  
  //Translate this angle in variations of tx and ty, tan(alpah) = x/z = tx (Good enough approximation also for the 2D scattering case)
  double txRms2 = pow((1+pow((state->GetStateVect())[2],2))*angleRms,2);
  double tyRms2 = pow((1+pow((state->GetStateVect())[3],2))*angleRms,2);//TODO
  
  //Noise matrix to be added to the covariance
  double q[5*5]={0,0,       0,       0,0,
                 0,0,       0,       0,0,
                 0,0,  txRms2,       0,0,
                 0,0,       0,  tyRms2,0,
                 0,0,       0,       0,0};
  
  TMatrixD Q(5,5,q);
  
  state->SetP(state->GetP() + Q);
  
  return true;
}





