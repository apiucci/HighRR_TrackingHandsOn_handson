
/*!
 *  @author    Alessio Piucci
 *  @brief     Library to implement chi2 fit
 */

//custom libraries
#include "TFRChiSquaredFit.h"

//----------
// Perform a 1D linear fit 
//----------
bool TFRChiSquaredFit::FitLinear1D(TFRTrack *track, bool xz_view){

  //------------------------------//
  // compute the track parameters //
  //------------------------------//
  
  double S1   = 0.;
  double Sz   = 0.;
  double Szz  = 0.;
  double Sx   = 0.;
  double Sxz  = 0.;
  
  //x (oy y) and z positions of the cluster
  double coord_cluster, z_cluster;
  double cluster_err_sq;

  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;
  
  //loop over the clusters of the track
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){

    //should I fit x or y position of the clusters?
    if(xz_view){
      coord_cluster = curr_cluster->GetX();
      cluster_err_sq = curr_cluster->GetCovariance()(0, 0);
    }
    else{
      coord_cluster = curr_cluster->GetY();
      cluster_err_sq = curr_cluster->GetCovariance()(1, 1);
    }
    
    z_cluster = curr_cluster->GetZ();
    
    S1  += 1. / cluster_err_sq;
    Sz  += z_cluster / cluster_err_sq;
    Szz += pow(z_cluster, 2.) / cluster_err_sq;
    Sx  += coord_cluster / cluster_err_sq;
    Sxz += (coord_cluster * z_cluster) / cluster_err_sq;
    
  }  //loop over the clusters of the track
  
  double D = S1*Szz - pow(Sz, 2.);
  
  //check if everything went fine
  if(!(std::fabs(D) > 1e-5)){
    std::cout << "TFRChiSquaredFit::Error: D = " << D << std::endl;
    return false;
  }
  
  double a = (Sx*Szz - Sz*Sxz) / D;
  double b = (S1*Sxz - Sz*Sx) / D;

  //set the track linear parameters
  if(xz_view)
    track->SetTrackParamsXZ(a, b);
  else
    track->SetTrackParamsYZ(a, b);
  
  //------------------//
  // compute the chi2 //
  //------------------//

  double chi2 = 0.;

  //reset the iterator, to loop again over the clusters
  it_cluster = it_cluster.Begin();

  //loop over the clusters of the track
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){

    //should I fit x or y position of the clusters?
    if(xz_view){
      coord_cluster = curr_cluster->GetX();
      cluster_err_sq = curr_cluster->GetCovariance()(0, 0);
    }	
    else{
      coord_cluster = curr_cluster->GetY();
      cluster_err_sq = curr_cluster->GetCovariance()(1, 1);
    }
    
    z_cluster = curr_cluster->GetZ();
    
    //compute the chi2
    chi2 = pow(coord_cluster - a - b*z_cluster, 2.) / cluster_err_sq;
    
  }  //loop over the clusters of the track
  
  //set the track Chi2
  track->SetChi2(chi2);
  track->SetChi2NdoF(chi2/(track->GetNClusters() - 2.));

  //TO DO: SET THE COVARIANCE MATRIX!
  
  return true;
}

//----------
// Perform a 1D linear fit, with matrix formalism
//----------
bool TFRChiSquaredFit::FitLinear1D_matrix(TFRTrack *track, bool xz_view,
					  TFRGeometry *detector_geo){

  //linear formalism:
  // f = Ap
  //where:
  // A = (1, z0)
  //     (1, z1)
  //     (1, ..)
  //     (1, zn)
  // p = (a)
  //     (b)

  //X is the array of measurements
  // X = (x0)
  //     (..)
  //     (xn)

  //V is the covariance matrix of the measurements
  // V_ij = < (x_i - <x_i>)*(x_j - <x_j>) >
  // in case of uncorrelated measurements, it is:
  // V_id = sigma_ij^2 * delta_ij
  
  //the minimization of the chi2 gives the parameters:
  // p' = (At W A)-1  At  W  X
  // (t = trans. matrix, W = V-1)
  //the covariance matrix of the parameters is:
  // Vp = (At W A)-1
  
  //get the number of hits of the track
  unsigned int num_hits = track->GetNClusters();
  
  //A matrix: 2*num_hits dimensionality
  TMatrixD A(num_hits, 2);

  //X vector of measurements: num_hits dimensionality
  TVectorD X(num_hits);
  
  //V covariance matrix of the measurements: num_hits*num_hits dimensionality
  TMatrixD V(num_hits, num_hits);
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;

  //index to fill the matrices and vectors
  unsigned int i = 0;
  
  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){

    //set the A matrix
    A(i, 0) = 1.;
    A(i, 1) = curr_cluster->GetZ();
    
    //set the X vector of measurements

    //should I fit the x or y position of the clusters?
    if(xz_view){
      X(i) = curr_cluster->GetX();
      V(i, i) = curr_cluster->GetCovariance()(0, 0);
    }
    else{
      X(i) = curr_cluster->GetY();
      V(i, i) = curr_cluster->GetCovariance()(1, 1); 
    }  //if(xz_view)

    //should I take in account the multiple scattering?
    if(detector_geo->GetMultipleScattStatus()){

      //assume momentum and Kaon mass...
      double momentum	= 5000.;
      double beta = sqrt(1. - pow(493.7/momentum, 2));
      
      //the first cluster is not effected
      if(i != 0){
	V(i, i) += (pow(A(i, 1) - A(i - 1., 1), 2.)
		    * pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(curr_cluster->GetLayer().GetXoverXZero(),
										  beta, momentum, 1),
			  2.));
	
	//let's forget the off-diagonal elements, for now...
	
      }  //if(i != 0)
    }  //if(detector_geo->GetMultipleScattStatus())
    
    ++i;
    
  }  //loop over the clusters   
  
  //invert the V covariance matrix
  TMatrixD W(TMatrixD::kInverted, V);
  
  //transpose the A matrix
  TMatrixD At(TMatrixD::kTransposed, A);
    
  //compute the V_p covariance matrix of the parameters
  TMatrixD V_p(TMatrixD::kInverted, At * W * A); 
  
  //compute the track parameters
  TVectorD P(2);
  
  P = V_p * At * W * X;
  
  //set the track parameters
  if(xz_view)
    track->SetTrackParamsXZ(P(0), P(1));
  else
    track->SetTrackParamsYZ(P(0), P(1));
  
  //------------------------------------------//
  //  set the covariance matrix of the track  //
  //------------------------------------------//

  //in the linear chi2 fit the covariance matrix is 2x2,
  //but I have to fill a 3x3 matrix
  TMatrixD V_p_temp(3, 3);

  V_p_temp.SetSub(0, 0, V_p);
  
  //finally set the track covariance matrix
  track->SetCovarianceMatrixXZ(V_p_temp);
  
  //--------------------//
  //  compute the chi2  //
  //--------------------//

  //chi2 = (X - (A * P))t * W * (X - (A * P))
  TVectorD column = X - (A * P);
  
  double chi2 = column * (W * column);
  
  //set the track chi2
  track->SetChi2(chi2);
  track->SetChi2NdoF(chi2/(track->GetNClusters() - 2.));
  
  return true;
}

//----------
// Perform a 1D quadratic fit, with matrix formalism
//----------
bool TFRChiSquaredFit::FitQuadratic1D_matrix(TFRTrack *track, bool xz_view,
					     TFRGeometry *detector_geo){

  //almost the same of the linear case, with the only difference of the dimensionality:
  // f = Ap
  //where:
  // A = (1, z0, z0^2)
  //     (1, z1, z1^2)
  //     (1, .., ..  )
  //     (1, zn, zn^2)
  // p = (a)
  //     (b)
  //     (c)
  //where c = -1./(2R), with R = p/(0.3*q*B)
  
  //get the number of hits of the track
  unsigned int num_hits = track->GetNClusters();
  //A matrix: 3*num_hits dimensionality
  TMatrixD A(num_hits, 3);
  
  //X vector of measurements: num_hits dimensionality
  TVectorD X(num_hits);

  //V covariance matrix of the measurements: num_hits*num_hits dimensionality
  TMatrixD V(num_hits, num_hits);

  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;

  //index to fill the matrices and vectors
  unsigned int i = 0;

  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){
    
    //set the A matrix
    A(i, 0) = 1.;
    A(i, 1) = curr_cluster->GetZ();
    A(i, 2) = pow(curr_cluster->GetZ(), 2.);
    
    //set the X vector of measurements
    
    //should I fit the x or y position of the clusters?
    if(xz_view){
      X(i) = curr_cluster->GetX();
      V(i, i) = curr_cluster->GetCovariance()(0, 0);
    }
    else{
      X(i) = curr_cluster->GetY();
      V(i, i) = curr_cluster->GetCovariance()(1, 1);
    }
    
    //should I take in account the multiple scattering?
    if(detector_geo->GetMultipleScattStatus()){

      //assume momentum and Kaon mass...
      double momentum   = track->GetMomentumMod();
      double beta = sqrt(1. - pow(493.7/momentum, 2));
     
      for(int j = i-1; j>=0; j--){
        V(i, i) += (pow(A(i, 1) - A(j, 1), 2.)
                    * pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At(j))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.));
      }//for(int j = i-1; j>=0; j--){
    }  //if(detector_geo->GetMultipleScattStatus())
    
    ++i;
    
  }  //loop over the clusters
  
  //invert the V covariance matrix
  TMatrixD W(TMatrixD::kInverted, V);
  
  //transpose the A matrix
  TMatrixD At(TMatrixD::kTransposed, A);
  
  //compute the V_p covariance matrix of the parameters
  TMatrixD V_p(TMatrixD::kInverted, At * W * A);
  
  //compute the track parameters
  TVectorD P(3);
  
  P = V_p * At * W * X;

  //set the track parameters and covariance matrix
  if(xz_view){
    track->SetTrackParamsXZ(P(0), P(1), P(2));
    track->SetCovarianceMatrixXZ(V_p);
  }  
  else{
    track->SetTrackParamsYZ(P(0), P(1), P(2));
    track->SetCovarianceMatrixYZ(V_p);
  }
  
  //--------------------//
  //  compute the chi2  //
  //--------------------//
  
  //chi2 = (X - (A * P))t * W * (X - (A * P))
  TVectorD column = X - (A * P);
  
  double chi2 = column * (W * column);
  
  //set the track chi2
  track->SetChi2(chi2);
  track->SetChi2NdoF(chi2/(track->GetNClusters() - 3.));
  
  return true;
}

//----------
// Perform a 2D linear fit, with matrix formalism
//----------
bool TFRChiSquaredFit::FitLinear2D_matrix(TFRTrack *track,
					  TFRGeometry *detector_geo){
  
  //linear formalism:                                                                                                                                                                              
  // f = Ap
  //where:
  // A = (1, z0, 0,  0)
  //     (0,  0, 1, z0)
  //     (1, z1, 0,  0)
  //     (0,  0, 1, z1)
  //     (............)
  //     (1, zn, 0,  0)
  //     (0,  0, 1, zn)
  // p = (a_x)
  //     (b_x)
  //     (a_y)
  //     (b_y)
  
  //X is the array of measurements
  // X = (x0)
  //     (y0)
  //     (..)
  //     (xn)
  //     (yn)
  
  //V is the covariance matrix of the measurements
  // V_ij = < (x_i - <x_i>)*(x_j - <x_j>) >
  // in case of uncorrelated measurements, it is:
  // V_id = sigma_ij^2 * delta_ij
  //
  // it should look like
  // V = ( V_0 -----   0  -------- 0 )
  //     (  0   V_1    0  -------- 0 )
  //     ( ------------------------ )
  //     (  0           0   V_xn   0  )
  //     (  0           0        V_yn )
  // where V_xi and V_yj are the covariance matrices
  // of the x and j measurements
  
  //the minimization of the chi2 gives the parameters:
  // p' = (At W A)-1  At  W  X
  // (t = trans. matrix, W = V-1)

  //the covariance matrix of the parameters is:
  // Vp = (At W A)-1
  
  //get the number of hits of the track
  unsigned int num_hits = track->GetNClusters();

  //A matrix: 2*num_hits dimensionality
  TMatrixD A(num_hits*2, 4);

  //X vector of measurements: num_hits dimensionality
  TVectorD X(num_hits*2);

  //V covariance matrix of the measurements: num_hits*num_hits dimensionality
  TMatrixD V(num_hits*2, num_hits*2);
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;

  //index to fill the matrices and vectors
  unsigned int i = 0;

  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){
    
    //set the A matrix
    A(i*2    , 0) = 1.;
    A(i*2    , 1) = curr_cluster->GetZ();
    A(i*2 +1 , 2) = 1.;
    A(i*2 + 1, 3) = curr_cluster->GetZ();

    //set the X vector of measurements
    X(i*2    ) = curr_cluster->GetX();
    X(i*2 + 1) = curr_cluster->GetY();
    
    //let's assume uncorrelated measurements on x and y
    V.SetSub(i*2, i*2, curr_cluster->GetCovariance());
    //should I take in account the multiple scattering?
    if(detector_geo->GetMultipleScattStatus()){

      //assume Kaon mass, and momentum from some previous estimation
      double momentum   = track->GetMomentumMod();
      double beta = sqrt(1. - pow(493.7/momentum, 2));
      
      //Diagonal elements
      //x 
      for(int j = 2*i-2; j>=0; j=j-2){
        V(2*i, 2*i) += (pow(A(2*i, 1) - A(j, 1), 2.)
                    * pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At(j/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.));
      }//for(int j = i-1; j>=0; j--){
      //y 
      for(int j = 2*i+1-2; j>=0; j=j-2){
        V(2*i+1, 2*i+1) += (pow(A(2*i+1, 3) - A(j, 3), 2.)
                    * pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At((j-1)/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.));
      }//for(int j = i-1; j>=0; j--){
      //off-diagonal elements
      //x
      for(int j = 2*i-2; j>=0; j=j-2){
        for(int l = j-2; l>=0; l=l-2){
          V(2*i,j)+=(A(2*i, 1) - A(l, 1))*(A(j, 1) - A(l, 1))*pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At(l/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.);
          V(j,2*i)+=(A(2*i, 1) - A(l, 1))*(A(j, 1) - A(l, 1))*pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At(l/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.);
        }//for(int l = j-2; l>=0; l=l-2)
      }//for(int j = i-2; j>=0; j=j-2)
      //y
      for(int j = 2*i+1-2; j>=0; j=j-2){
        for(int l = j-2; l>=0; l=l-2){
          V(2*i+1,j)+=(A(2*i+1, 3) - A(l, 3))*(A(j, 3) - A(l, 3))*pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At((l-1)/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.);
          V(j,2*i+1)+=(A(2*i+1, 3) - A(l, 3))*(A(j, 3) - A(l, 3))*pow(detector_geo->GetMultipleScattering()->GetThetaRMSGauss(((TFRCluster*)track->GetClusters()->At((l-1)/2))->GetLayer().GetXoverXZero(),
                                                                                  beta, momentum, 1),
                          2.);
        }//for(int l = j-2; l>=0; l=l-2)
      }//for(int j = i-1; j>=0; j--){
    }  //if(detector_geo->GetMultipleScattStatus())

    //to write the multiple scattering contribution to the covariance!
    
    ++i;
    
  }  //loop over the clusters
  
 
  //invert the V covariance matrix
  TMatrixD W(TMatrixD::kInverted, V);
  
  //transpose the A matrix
  TMatrixD At(TMatrixD::kTransposed, A);

  //compute the V_p covariance matrix of the parameters
  TMatrixD V_p(TMatrixD::kInverted, At * W * A);

  //compute the track parameters
  TVectorD P(4);
  
  P = V_p * At * W * X;
  
  //set the track parameters
  track->SetTrackParamsXZ(P(0), P(1));
  track->SetTrackParamsYZ(P(2), P(3));
  
  //here the covariance matrix is 4x4,
  //but I need to fill the full 6x6 covariance matrix
  TMatrixD V_p_temp(6, 6);

  V_p_temp.SetSub(0, 0, V_p.GetSub(0, 1, 0, 1));
  V_p_temp.SetSub(3, 3, V_p.GetSub(2, 3, 2, 3));
  
  //set the covariance matrix of the track
  track->SetCovarianceMatrix(V_p_temp);

  //--------------------//
  //  compute the chi2  //
  //--------------------//
  
  //chi2 = (X - (A * P))t * W * (X - (A * P))
  TVectorD column = X - (A * P);
  double chi2 = column * (W * column);
  
  //set the track chi2
  track->SetChi2(chi2);
  track->SetChi2NdoF(chi2/(track->GetNClusters()*2 - 4.));

  return true;
}
