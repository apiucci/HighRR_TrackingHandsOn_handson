#ifndef INCLUDE_TFRSTATE_H
#define INCLUDE_TFRSTATE_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a track state
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TVectorD.h"
#include "TMatrixD.h"

using namespace std;

class TFRState : public TObject {
 public:

  /*! Empty constructor */
  TFRState(){
    state_vect.ResizeTo(5);
    state_vect = TVectorD(5);
    P.ResizeTo(5,5);
    P = TMatrixD(5,5);
    chi2=0;};

  /*! Standard constructor */
  TFRState(TVectorD _state_vect, double _z,
	   double _chi2 = 999999., TMatrixD _P = TMatrixD(5,5)) :
    state_vect(_state_vect), z(_z), chi2(_chi2), P(_P) {}
    
  /*! Destructor */
  virtual ~TFRState(){ };

  /*! Set the state vector using an array*/
  inline void SetStateVect(const double x[5]){
    state_vect.SetElements(x);};

  /*! Set the state vector using a TVectorD*/
  inline void SetStateVect(const TVectorD vect){
    state_vect = vect;};
  
  /*! Set the covariance matrix using an array*/
  inline void SetP(const double _P[25]){
    P = TMatrixD(5,5,_P);};
  
  /*! Set the covariance matrix using a TMatrixD*/
  inline void SetP(const TMatrixD cov){
    P = cov;};
  
  /*! Set the chi2 of the state (if it is associated to a measurement, e.g. in a TFRFitnode) */
  inline void SetChi2(const double _chi2){chi2=_chi2;};

  /*! Update the tx and ty given a momentum vector*/
  inline void UpdateTxTy(const TVectorD momentum){
    state_vect[2] = momentum[0]/momentum[2];  //tx
    state_vect[3] = momentum[1]/momentum[2];  //ty
  };
  
  /*! Returns the state vector */
  inline TVectorD GetStateVect() const {return state_vect;};

  /*! Returns the x position */
  inline double GetX() const {return state_vect[0];};

  /*! Returns the y position */
  inline double GetY() const {return state_vect[1];};

  /*! Returns tx */
  inline double GetTx() const {return state_vect[2];};

  /*! Returns ty */
  inline double GetTy() const {return state_vect[3];};
    
  /*! Returns the momentum vector */
  inline TVectorD GetMomentum() const {
    double pz = 1./
                  (fabs(this->GetQoP())
                   * sqrt(pow(this->GetTx(), 2.) + pow(this->GetTy(), 2) + 1));
    double momentum[3] = {this->GetTx() * pz,
		    this->GetTy() * pz,
		    pz};
    return TVectorD(3,momentum);
  };
  
  /*! Returns q over p */
  inline double GetQoP() const {return state_vect[4];};
    
  /*! Returns the z position of the state */
  inline double GetZ() const {return z;};
  
  /*! Sets the z position of the state */
  inline double SetZ(const double zval) {return z = zval;};

  /*! Returns the chi2 of the state (if it is associated to a measurement, e.g. in a TFRFitnode) */
  inline double GetChi2() const {return chi2;};

  /*! Returns the covariance matrix of the state */
  inline TMatrixD GetP(){return P;};
  
 protected:

 private:

  /*! Vector representig the state */
  TVectorD state_vect;

  /*! z position of the state */ 
  double z;

  /*! Chi2 of the state */
  double chi2;

  /*! P matrix of the state */
  TMatrixD P;
  
  //magic happens here
  ClassDef(TFRState, 1)
};

/*! Array of track states */
typedef TObjArray TFRStates;

#endif // INCLUDE_TFRSTATE_H
